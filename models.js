var mongoose = require('mongoose')
	Schema = mongoose.Schema;

var ImageSchema = new Schema({
	title: String,
	link: String,
	topic: String
});

ImageSchema.index({ topic: 'text' });

module.exports = mongoose.model('Image', ImageSchema);