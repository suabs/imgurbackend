'use strict';

var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;

var mongoose = require('mongoose');
require('sinon-mongoose');

//Importing model for our unit testing.
var ImageSchema = require('../models');

describe('Database connection', function () {
	var connection;

	beforeEach(function (done) {
		connection = mongoose.createConnection(process.env.MONGO_URL || 'mongodb://127.0.0.1/imgur-test');
		connection.once('open', function () {
			done();
		});
	});

	afterEach(function (done) {
		connection.close(function () {
			done();
		});
	});

	it('Register schema', function (done) {
		var Image = mongoose.model('Image', ImageSchema);
		done();
	});
});


describe("Test /api/imgur", function(done) {
	it ("should remove all the old images from the database", function(done) {
		var ImageMock = sinon.mock(ImageSchema);
		var expectedResult = { status: true };
		ImageMock.expects('remove').withArgs({}).yields(null, expectedResult);
		ImageSchema.remove({}, function(err, result) {
			ImageMock.verify();
			ImageMock.restore();
			expect(result.status).to.be.true;
			done();
		});
	});

	it ("should return error if remove action failed", function(done) {
		var ImageMock = sinon.mock(ImageSchema);
		var expectedResult = { status: false };
		ImageMock.expects('remove').withArgs({}).yields(expectedResult, null);
		ImageSchema.remove({}, function(err, result) {
			ImageMock.verify();
			ImageMock.restore();
			expect(err.status).to.not.be.true;
			done();
		});
	});

	it("should save all new viral images", function(done){
		var ImageMock = sinon.mock(new ImageSchema({ 
			title: 'Today is good day',
			link: 'http://google.com',
			topic: 'Hello world'
		}));

		var image = ImageMock.object;
		var expectedResult = { status: true };
		ImageMock.expects('save').yields(null, expectedResult);
		image.save(function (err, result) {
			ImageMock.verify();
			ImageMock.restore();
			expect(result.status).to.be.true;
			done();
		});
	});	

	it("should return error if saving images failed", function(done){
		var ImageMock = sinon.mock(new ImageSchema({ 
			title: 'Today is good day',
			link: 'http://google.com',
			topic: 'Hello world'
		}));

		var image = ImageMock.object;
		var expectedResult = { status: false };
		ImageMock.expects('save').yields(expectedResult, null);
		image.save(function (err, result) {
			ImageMock.verify();
			ImageMock.restore();
			expect(err.status).to.not.be.true;
			done();
		});
	});	

});
