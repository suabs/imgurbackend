Imgur Backend API
=================================================================
This API gets the latest viral images from the imgur. It inserts them to mongodb database. It also provides text search for topics field to the saved data.


### Requirements
* npm and Nodejs

### Run this API
```
$ npm install
$ npm install -g nodemon
$ nodemon server.js

```

### Deploy to heroku
```
$ heroku git:remote -a imgurbacked
$ git push heroku master
$ heroku addons:create mongolab

```

#### [Heroku Demo](https://imgurbacked.herokuapp.com/)

### Source
* [https://scotch.io/tutorials/creating-a-single-page-todo-app-with-node-and-angular](https://scotch.io/tutorials/creating-a-single-page-todo-app-with-node-and-angular)
* [http://mongoosejs.com/docs/guide.html](http://mongoosejs.com/docs/guide.html)
* [https://api.imgur.com/](https://api.imgur.com/)