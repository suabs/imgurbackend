var express = require('express');
var app = express();
var cors = require('cors');
var mongoose = require('mongoose');
var bodyparser = require('body-parser');
var request = require('request');

var Images = require('./models')

mongoose.connect('mongodb://imgurbackend:imgurbackend@ds063725.mlab.com:63725/heroku_ll7bqgpr');

//Enable Cross Origin Resource Sharing
app.use(cors());
app.use(bodyparser.urlencoded({'extended':'true'}));
app.use(bodyparser.json());



var imageData = {
	url : "https://api.imgur.com/3/gallery/hot/viral/0.json",
	headers: {
		'Authorization': 'Client-ID 982342cf1f65941'
	}
}

app.get('/api/imgur', function(req,res){

	Images.remove({}, function(err){
		if(err){
			return res.json(err);
		}
		console.log('Removed successfully...');		
	});

	request.get(imageData,function(err, response, gallery){
		if(err){
			return res.json(err);
		}		
		
		var images = JSON.parse(gallery).data;
		Images.create(images, function(err,gallery){
			if(err){
				return res.json(err);
			}			
			console.log("successfully inserted...");
			//return res.json(gallery);
		});
		return res.json(images);
	});

});

app.get('/api/imgur/:query', function(req,res){
	console.log(req.params.query);
	Images.find({
		$text: {$search: req.params.query}
	}, '-_id', function(err,gallery){
		if(err){
			return res.json(err);
		}
		return res.json(gallery);
	});
});

app.get('/', function(req,res){
	return res.json({'hello':'world'})
});

var port = process.env.PORT || 8080
app.listen(port);
console.log("Connection is established on port ...", port);
